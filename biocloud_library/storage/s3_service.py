import logging
from typing import Optional
import boto3
from botocore.client import Config


class S3Service:
    """Provides functionality for interacting with S3."""

    def __init__(self, access_key_id: str, secret_access_key: str, endpoint_url: str, region_name: str, max_pool_connections: int = 20):
        session = boto3.Session(aws_access_key_id=access_key_id,
                                aws_secret_access_key=secret_access_key,
                                region_name=region_name)
        self.s3 = session.client("s3", endpoint_url=endpoint_url, config=Config(signature_version="s3v4", max_pool_connections=max_pool_connections))

    def put_object(self, bucket: str, key: str, body: str) -> None:
        """Put an object in S3."""
        logging.info(f"Writing {key} to S3...")
        try:
            self.s3.put_object(Bucket=bucket, Key=key, Body=body)
            logging.info(f"Successfully wrote {key} to S3.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def get_object(self, bucket: str, key: str) -> str:
        """Get an object from S3."""
        logging.info(f"Fetching {key} from S3...")
        try:
            response = self.s3.get_object(Bucket=bucket, Key=key)
            logging.info(f"Successfully fetched {key} from S3.")
            return response["Body"].read().decode("utf-8")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def copy_object(self, source_bucket: str, destination_bucket: str, source_key: str, destination_key: str) -> None:
        """Copy an object from a source bucket to a destination bucket"""
        logging.info(f"Copying {source_bucket}/{source_key} to {destination_bucket}/{destination_key}")
        try:
            self.s3.copy_object(
                Bucket=destination_bucket,
                CopySource={"Bucket": source_bucket, "Key": source_key},
                Key=destination_key,
            )
            logging.info(f"Successfully copied {source_key} to {destination_key}.")
        except Exception as e:
            logging.error(f"Error found when copying {source_key}: {str(e)}")
            raise

    def download_file(self, bucket: str, key: str, filename: str, decode=True) -> str:
        """Download an object from S3 locally."""
        logging.info(f"Fetching {key} from S3 bucket {bucket} and storing into {filename}")
        try:
            self.s3.download_file(Bucket=bucket, Key=key, Filename=filename)
            logging.info(f"Successfully fetched {key} from S3 and stored into {filename}")
            return filename
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def upload_file(self, bucket: str, key: str, filename: str) -> None:
        """Upload a file to S3."""
        logging.info(f"Uploading {filename} to S3 bucket {bucket} as {key}.")
        try:
            self.s3.upload_file(Bucket=bucket, Key=key, Filename=filename)
            logging.info(f"Successfully uploaded {filename} to S3 as {key}.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def get_folders(self, bucket: str, prefix: str) -> list:
        """List folders within prefix. Prefix must end with '/'"""
        logging.info(f"Listing folders in {bucket} with prefix {prefix}...")
        folders = []
        if prefix[-1] != "/":
            prefix += "/"
        try:
            response = self.s3.list_objects(Bucket=bucket, Prefix=prefix, Delimiter='/')
            for item in response.get('CommonPrefixes'):
                folders.append(item.get('Prefix').split("/")[-2])
        except TypeError:
            logging.error(f"Object {response.get('CommonPrefixes')} is not iterable.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise
        return folders

    def exists_folder(self, bucket: str, prefix: str) -> bool:
        """Check if a folder exists in S3."""
        logging.info(f"Checking if {prefix} exists in S3...")
        try:
            response = self.s3.list_objects(Bucket=bucket, Prefix=prefix, Delimiter='/')
            if response.get('CommonPrefixes'):
                logging.info(f"{prefix} exists in S3.")
                return True
            else:
                logging.info(f"{prefix} does not exist in S3.")
                return False
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def exists_object(self, bucket: str, key: str) -> bool:
        """Check if an object exists in S3."""
        logging.info(f"Checking if {key} exists in S3...")
        try:
            self.s3.head_object(Bucket=bucket, Key=key)
            logging.info(f"{key} exists in S3.")
            return True
        except self.s3.exceptions.ClientError as e:
            if e.response['Error']['Code'] == '404':
                logging.info(f"{key} does not exist in S3.")
                return False
            else:
                logging.error(f"ClientError found: {str(e)}")
                raise
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def delete_object(self, bucket: str, key: str) -> None:
        """Delete an object from S3."""
        logging.info(f"Deleting {key} from S3...")
        try:
            self.s3.delete_object(Bucket=bucket, Key=key)
            logging.info(f"Successfully deleted {key} from S3.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def delete_objects(self, bucket: str, keys: list):
        """Delete objects from an S3 bucket. """
        #  Split the list of keys into chunks of 1000 for batch deletion
        chunk_size = 1000
        for i in range(0, len(keys), chunk_size):
            chunk = keys[i:i + chunk_size]
            # Prepare the format required by delete_objects
            objects_to_delete = [{'Key': key} for key in chunk]
            response = self.s3.delete_objects(
                Bucket=bucket,
                Delete={
                    'Objects': objects_to_delete,
                    'Quiet': True  # Change to False if you want to get the list of successful deletions
                }
            )
            logging.info(f"Deleted from S3 {response.get('Deleted', [])}")
            errors = response.get('Errors', [])
            if errors:
                logging.error(f"Errors while deleting from S3: {errors}")

    def list_objects(self, bucket: str, prefix: Optional[str] = None, only_files: bool = False) -> list:
        """List objects in S3. It lists both files and folders, unless <only_files> is set to True."""
        logging.info(f"Listing objects in {bucket} with prefix {prefix}...")
        files = []
        try:
            paginator = self.s3.get_paginator("list_objects_v2")
            response = paginator.paginate(Bucket=bucket, Prefix=prefix, PaginationConfig={"PageSize": 1000})
            for page in response:
                page_contents = page.get("Contents")
                if page_contents:
                    for file in page_contents:
                        if only_files and file["Key"].endswith("/"):
                            continue  # skip folders
                        files.append(file)
            logging.info(f"Successfully listed objects in {bucket} with prefix {prefix}.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise
        return files
