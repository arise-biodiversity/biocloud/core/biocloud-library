# Biocloud-library

Package library to share common code between biocloud repos.

## Packages included

- ### Biocloud S3 service

   Provides functionality for interacting with S3 such as put, get, delete and list objects from an S3 bucket.

## __Install and import the libraries where you want to use them__

   ```bash
   pip install biocloud-library --index-url https://gitlab.com/api/v4/projects/46492250/packages/pypi/simple
   ```

   It is also possible to [install using a _requirements.txt_](https://docs.gitlab.com/ee/user/packages/pypi_repository/#using-requirementstxt) file adding the following 2 lines at the end of the _requirements.txt_ file:

   ```text
   --extra-index-url https://gitlab.com/api/v4/projects/46492250/packages/pypi/simple
   biocloud-library
   ```

### Import library in the code

   ```python
   from biocloud_library.storage import S3Service
   from biocloud_library.config import Config
   ```

__NOTE__: Check that you have a .env file set up with the next required values:

```text
S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=
S3_ENDPOINT=
```

&nbsp;

---

## __Instructions for editing or adding more libraries__

## Installation

### __Clone Biocloud Library repository__

```bash
git clone git@gitlab.com:arise-biodiversity/biocloud/core/biocloud-library.git
```

### __Install [Poetry](https://python-poetry.org/docs/#installing-with-the-official-installer)__ and run the `list` command to show  the list of poetry commands

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

```bash
poetry list
```

### Check the packages installed in poetry

```bash
poetry show
```

### Add dependency packages if needed by the biocloud library with the command `poetry add`

For example,

```bash
poetry add python-dotenv boto3
```

That will update the dependencies in the `pyproject.toml` file

&nbsp;

## Development

### 1. Setup a virtual environment with `poetry` and enter it

```bash
poetry install
poetry shell
```

All the requirements are installed when entering the _poetry shell_.

### 2. Create .env file

Create .env file if it does not exist and check that the needed environment variables are in place.

```bash
cp .env.example .env
```

- __NOTE:__ If new environment variables are needed and added to this file, they also have to be added into the biocloud_library/config.py file.

### 3. Add libraries or make changes to current libraries

### 4. Update package information in this [document](#packages-included) if new libraries where added

&nbsp;

### Linting

```bash
poetry run flake8 --ignore=E501 --verbose .
```

&nbsp;

### __Testing with Pytest__

- Create tests and run pytest to check that the libraries work correctly.

- Install test requirements:

   Make sure that the test requirements are added to the `pyproject.toml` file as _dev_ dependencies before the tests can be run.

   Install the packages if needed by the biocloud library to perform tests:

   > - _`pytest`_ for testing the library
   > - _`testcontainers-minio`_ to connect with the test minio bucket.

   ```bash
   poetry add --dev pytest testcontainers-minio
   ```

&nbsp;

### Run the tests

```bash
poetry run pytest
```

&nbsp;

### In case of docker-error when running the tests

For the tests to run, you need to make sure docker daemon is running in the background. If you encounter the following error:
docker.errors.DockerException: Error while fetching server API version: ('Connection aborted.', FileNotFoundError(2, 'No such file or directory'))
Check the following things:

Make sure docker is running:

```bash
docker --version
```

If you are running docker desktop, it might be the result of a missing symbolic link. In that case, check if the following
file exists:

```bash
ls -l $HOME/.docker/desktop/docker.sock
```

(a message similar to this is a good result: srwxrwxr-x 1 leon leon 0 aug 3 14:08 /home/leon/.docker/desktop/docker.sock).

If the file exists, create a symbolic link:

```bash
sudo ln -s $HOME/.docker/desktop/docker.sock /var/run/docker.sock.
```

If that still fails, try modifying the rights:

```bash
sudo chmod a+rw /var/run/docker.sock.
```

&nbsp;

---

## Build the package

Create a source distribution and a wheel:

```bash
poetry build
```

Add the twine package if you don't have it already, to check if the build went well:

```bash
poetry add twine
poetry run twine check dist/*
```

Before publishing, update the biocloud library version number, for example:

```bash
poetry version patch
```

To view the options for setting a version number check [poetry version](https://python-poetry.org/docs/cli/#version)

&nbsp;

## Publish the package

> ###  __NOTE__: It is not possible to publish a package if a package of the same _name_ and _version_ already exists. Make sure that the package does not exist in the registry or change the version in the `pyproject.toml` file

&nbsp;

__The gitlab pipeline is already configured to build and publish the package automatically when merging to main__.

However, the steps to publish manually, if needed, are as follow:

1. First, install _twine_.

   ```bash
   pip install twine
   ```

2. Upload package to gitlab with _twine_. It can be done in two ways: inline or with `.pypirc` file.

   - __NOTE:__ Token credentials can be found in bitwarden

   A. Inline authentication

   ```bash
   TWINE_PASSWORD=<token> TWINE_USERNAME=<token_username> python3 -m twine upload --repository-url https://gitlab.com/api/v4/projects/46492250/packages/pypi dist/*
   ```

   B. Authentication with `.pypirc` file:

   - Create a [`~/.pypirc`](https://docs.gitlab.com/ee/user/packages/pypi_repository/#authenticate-with-the-package-registry) file, to authenticate, with the following information:

   ```text
   [distutils]
   index-servers =
      gitlab

   [gitlab]
   repository = https://gitlab.com/api/v4/projects/46492250/packages/pypi
   username = <token username>
   password = <token>
   ```

> NOTE: _46492250_ is the project id in gitlab

- Then upload the package:

   ```bash
   python3 -m twine upload --repository gitlab dist/*
   ```

&nbsp;

---

## Links

- Biocloud Documentation: [Biocloud Wiki](https://gitlab.com/groups/arise-biodiversity/biocloud/-/wikis/1.-Overview)

- More information about [__Poetry__](https://python-poetry.org/docs/cli/)

## License

[Apache License, version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
