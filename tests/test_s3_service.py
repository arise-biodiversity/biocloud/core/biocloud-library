import pytest
import json
from tests.minio_test_container import MinioTestContainer
from biocloud_library.config import Config
from biocloud_library.storage import S3Service


@pytest.fixture(scope="class")
def s3_service() -> S3Service:
    """Fixture for S3Service test class."""
    access_key = "admin"
    secret_key = "password"
    port = 9000

    container = MinioTestContainer(access_key=access_key, secret_key=secret_key, port_to_expose=port)
    container.start()

    client = container.get_client()
    client.make_bucket(Config.S3_LANDING_ZONE_BUCKET)
    client.make_bucket("copy-bucket")

    config = container.get_config()

    s3_service = S3Service(access_key, secret_key, 'http://' + config["endpoint"], Config.S3_REGION_NAME)

    yield s3_service

    container.stop()


@pytest.fixture(scope="class")
def test_object() -> str:
    """Test object for S3Service tests."""
    return json.dumps({"test": "test"})


@pytest.mark.usefixtures("s3_service", "test_object")
class TestS3Service:
    """Test the S3Service class."""

    def test_put_object(self, s3_service: S3Service, test_object: str) -> None:
        """Test putting an object to S3."""
        bucket_name = Config.S3_LANDING_ZONE_BUCKET
        file_name = "test.json"

        if s3_service.exists_object(bucket_name, file_name):
            s3_service.delete_object(bucket_name, file_name)

        s3_service.put_object(bucket_name, file_name, test_object)

        retrieved_file = s3_service.get_object(bucket_name, file_name)

        object_exists = s3_service.exists_object(bucket_name, file_name)
        s3_service.delete_object(bucket_name, file_name)

        assert retrieved_file == test_object
        assert object_exists is True

    def test_list_and_retrieve_object(self, s3_service: S3Service, test_object: str) -> None:
        """Test listing objects in a bucket and retrieving an object from S3."""
        bucket_name = Config.S3_LANDING_ZONE_BUCKET
        prefix = "test/"
        file1_name = f"{prefix}test1.json"
        file2_name = f"{prefix}test2.json"

        if s3_service.exists_object(bucket_name, file1_name):
            s3_service.delete_object(bucket_name, file1_name)
        if s3_service.exists_object(bucket_name, file2_name):
            s3_service.delete_object(bucket_name, file2_name)
        if s3_service.exists_object(bucket_name, prefix):
            s3_service.delete_object(bucket_name, prefix)

        s3_service.put_object(bucket_name, file1_name, test_object)
        s3_service.put_object(bucket_name, file2_name, test_object)
        s3_service.put_object(bucket_name, prefix, "")  # puts a folder

        file_objects = s3_service.list_objects(bucket_name, prefix, only_files=True)

        for obj in file_objects:
            file_contents = s3_service.get_object(bucket_name, obj['Key'])
            assert file_contents == test_object

        all_objects = s3_service.list_objects(bucket_name, prefix)  # includes folders

        s3_service.delete_object(bucket_name, file1_name)
        s3_service.delete_object(bucket_name, file2_name)
        s3_service.delete_object(bucket_name, prefix)

        assert len(file_objects) == 2
        assert len(all_objects) == 3

    def test_copy_object(self, s3_service: S3Service, test_object: str) -> None:
        """Test copying an object from a bucket into a bucket"""
        bucket_name = Config.S3_LANDING_ZONE_BUCKET
        copy_bucket = "copy-bucket"

        file_name = "test.json"
        destination_file = "destination.json"

        if s3_service.exists_object(bucket_name, file_name):
            s3_service.delete_object(bucket_name, file_name)
        if s3_service.exists_object(copy_bucket, destination_file):
            s3_service.delete_object(copy_bucket, destination_file)

        s3_service.put_object(bucket_name, file_name, test_object)

        s3_service.copy_object(bucket_name, copy_bucket, file_name, destination_file)

        object_exists = s3_service.exists_object(copy_bucket, destination_file)

        s3_service.delete_object(bucket_name, file_name)
        s3_service.delete_object(copy_bucket, destination_file)

        assert object_exists is True

    def test_delete_objects(self, s3_service: S3Service, test_object: str) -> None:
        bucket_name = Config.S3_LANDING_ZONE_BUCKET
        files = [f"delete-{i}.json" for i in range(0, 1100)]
        for file in files:
            s3_service.put_object(bucket_name, file, test_object)

        s3_service.delete_objects(bucket_name, files)
        for file in files:
            assert s3_service.exists_object(bucket_name, file) is False

    def test_delete_objects_empty_list(self, s3_service: S3Service, test_object: str) -> None:
        bucket_name = Config.S3_LANDING_ZONE_BUCKET
        files = []
        # should return with no errors
        s3_service.delete_objects(bucket_name, files)
